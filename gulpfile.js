/* jshint esversion: 6 */
const gulp = require('gulp');
const del = require('del');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const responsive = require('gulp-responsive');

gulp.task('copy-html', () => {
  return gulp
    .src('./index.html')
    .pipe(gulp.dest('./public'));
});

gulp.task('minify-css', () => {
  return gulp.src('css_src/*.css')
    .pipe(concat('style.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/resources/css'))
    .pipe(browserSync.stream());
});

gulp.task('clean-images', () => {
  return del(
             ['public/resources/images/**', '!public/resources/images'],
             { force: true }
             );
  });

gulp.task('copy-images', () => {
  return gulp
    .src('images_src/*.{jpg,png}')
    .pipe(
      responsive(
      {
        '*.jpg': [ { format: 'webp' } ],
        '*.png': [ { format: 'webp' } ]
      },
      {
        quality: 40,
        progressive: true,
        withMetadata: false,
        errorOnEnlargement: false
      })
    )
    .pipe(gulp.dest('public/resources/images'));
});

gulp.task('public', gulp.series(['copy-html',
                               'minify-css',
                               'clean-images',
                               'copy-images'
                               ]));

gulp.task('default', gulp.series(['copy-html',
                                  'minify-css',
                                  'clean-images',
                                  'copy-images'], () => {
  gulp.watch('css/**/*.css', gulp.series('minify-css'));
  gulp.watch('./index.html', gulp.series('copy-html'));
  gulp.watch('./public/index.html').on('change', browserSync.reload);
	browserSync.init({
    server: {
        baseDir: './public'
    }
  });

}));
